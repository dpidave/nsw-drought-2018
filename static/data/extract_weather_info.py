from bs4 import BeautifulSoup
import csv

# SET THIS
MONTH_INDEX = 3 # Arp
# JUST FOR REFERENCE
MON_SEL = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug',
           'Sep', 'Oct', 'Nov', 'Dec']


def get_yr_obs_data(s, year='2018'):
    '''
    s = soup
    year to target for data
    returns: list of strings
    '''
    # skip last value as that is the mean
    try:
        rain = [td.text for td in s.find('a', href=True, text=str(year)).find_parent('tr').findAll('td')[:-1]]
        return rain
    except AttributeError:
        return []

def get_yr_mean_data(s):
    '''
    s = soup
    returns: list of strings
    '''
    table = s.find('table', id='statsTable')
    # last value is the sum, so skip that
    try:
        rain_mean = [td.text for td in table.find('th', text='Mean').find_parent('tr').findAll('td')[:-1]]
        return rain_mean
    except AttributeError:
        return []

# get file paths for each lga
file_paths = {}
with open('lga_names_add_filepaths.csv') as f:
    csv_reader = csv.reader(f)
    for row in csv_reader:
        file_paths[row[0]] = row[1]

# get the weather info from the html pages
header = ['Jan_2017', 'Feb_2017', 'Mar_2017', 'Apr_2017', 'May_2017',
          'Jun_2017', 'Jul_2017', 'Aug_2017', 'Sep_2017', 'Oct_2017',
          'Nov_2017', 'Dec_2017', 'Jan_2018', 'Feb_2018', 'Mar_2018',
          'Apr_2018', 'May_2018', 'Jun_2018', 'Jul_2018', 'Aug_2018',
          'Sep_2018', 'Oct_2018', 'Nov_2018', 'Dec_2018', 'Mean_Jan',
          'Mean_Feb', 'Mean_Mar', 'Mean_Apr', 'Mean_May', 'Mean_Jun',
          'Mean_Jul', 'Mean_Aug', 'Mean_Sep', 'Mean_Oct', 'Mean_Nov',
          'Mean_Dec']
header.insert(0, 'name')

# lga:[2017, 2018, mean] listed in month order, '' is missing
weather_data = {}
for lga in file_paths:
    file_target = file_paths[lga]
    if file_target != '':
        with open(file_target) as f:
            soup = BeautifulSoup(f.read(), 'html.parser')
            obs_2017 = get_yr_obs_data(soup, '2017')
            obs_2018 = get_yr_obs_data(soup, '2018')
            mean = get_yr_mean_data(soup)
            weather_data[lga] = [obs_2017, obs_2018, mean]

outfile = open('lga_geoIDs_weather_data.csv', 'w')
csv_writer = csv.writer(outfile)
csv_writer.writerow(header)

for lga in weather_data:
    row = [lga]
    row = row + weather_data[lga][0]
    row = row + weather_data[lga][1]
    row = row + weather_data[lga][2]
    csv_writer.writerow(row)
outfile.close()

print('use spreadsheet to add some summary stats')
