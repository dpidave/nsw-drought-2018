#!/usr/bin/env python

import requests
import time
import random

#Opened: </strong>1905</div><div id="status"><strong>Now: </strong>Open</div></div>
#for ws in ws_list:
target_url = "http://www.bom.gov.au/jsp/ncc/cdio/weatherData/av?p_nccObsCode=139&p_display_type=dataFile&p_startYear=&p_c=&p_stn_num=%s"

def get_page(ws_id):
    ''' get BOM weather data by weather station id, return page as text'''
    page = requests.get(target_url%ws_id)
    if page.ok:
        return page.text
    else:
        return None

# create a dictionary of weather stations per locale in case of no-data
lga_dict = {}
with open('./lga_weather_stations.txt') as f:
    for line in f:
        bits = line.strip().split('\t')
        name = bits[0]
        stations = bits[1].split(';')
        lga_dict[name] = stations

time_delay = range(10,60)

for name in lga_dict:
    print(name)
    for station in lga_dict[name]:
        time.sleep(random.choice(time_delay))
        page = get_page(station)
        if page != None:
            #break
            opened = page[page.find('Opened:'):].split('>')[1].split('<')[0]
            closed = page[page.find('Now:'):].split('>')[1].split('<')[0]
            print(opened)
            print(closed)
            if int(opened) < 2010 and closed == 'Open':
                # save result and move along
                break
            else:
                # keep looking
                pass
        else:
            pass
    with open('%s.html'%(name), 'w') as f:
        try:
            f.write(page)
        except:
            f.write('')
