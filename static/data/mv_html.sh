#!/bin/bash

for h in *html; do
  if grep -q '>2018<' $h; then
    mv $h ./found
  else
    mv $h ./missing
  fi
done
