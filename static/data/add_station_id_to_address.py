#!/usr/bin/env python

import csv

station_data = {}
with open('./nsw_weather_stations.txt') as f:
    for line in f:
        bits = line.strip().split(' ')
        station = bits[0]
        locale = line[line.find(' '):line.find('-')].strip()
        #lat, longt = bits[2], bits[3]
        station_data[locale] = station

# get address information 
lga_dict = {}
with open('./lga_addresses.tsv') as f:
    for line in f:
        bits = line.strip().split('\t')
        name = bits[0]
        post = bits[2]
        lga_dict[name] = post

counter_fail = 0
counter_pass = 0

outfile = open('lga_weather_stations.txt', 'w')

for name in lga_dict:
    target = lga_dict[name]
    flag = 0
    stations = []
    for locale in station_data:
        if locale.find(target) >= 0:
            flag = 1
            stations.append(station_data[locale])
    if flag == 1:
        counter_pass += 1
    else:
        counter_fail += 1
        print target
    out = '%s\t%s\n' % (name, ';'.join(stations))
    outfile.write(out)
outfile.close()
