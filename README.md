# D3 based drought map of NSW July 2017 to 2018.  
The goal of this project was to lean D3.  

The result is a dynamic map of nsw local govenment areas with the following data:  
-  Percent of normal rainfall accumilation for a given month, from July 2017  
-  Average rainfall per month  

## useful examples:   
Really good example here: https://bl.ocks.org/john-guerra/43c7656821069d00dcbc  

## ToDo  
Think of a better metric than rainfall deficit as it is not obvious what this
is   

## Source  
Australian BOM weather station data  

## License  
MIT open source license, see license.txt for details.  
